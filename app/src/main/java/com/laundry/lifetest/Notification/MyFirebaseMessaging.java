package com.laundry.lifetest.Notification;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;
import com.laundry.lifetest.activities.HomeScreen;

import java.util.Map;

public class MyFirebaseMessaging extends FirebaseMessagingService {
    public static int NOTIFICATION_ID = 1;
    boolean is_noty = false;
    private UserSession session;

    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
        Log.e("newToken", token);
//Add your token in your sharepreferences.
        session = new UserSession(getApplicationContext());
      //  session.setFirbaseDeviceToken(token);

    }




    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e("newToken", "notify");

        sendNotification(remoteMessage.getData());

    }

    private void sendNotification(Map<String, String> data) {
        int num = ++NOTIFICATION_ID;

        //  Log.e("Type", msg.getString("type") + " " + msg.getString("id"));
        Intent backIntent;
        PendingIntent pendingIntent = null;

        session = new UserSession(getApplicationContext());

        backIntent = new Intent(getApplicationContext(), HomeScreen.class);
       // backIntent.putExtra("key", msg.getString("type"));
        backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = getString(R.string.app_name) + "channel_01";// The id of the channel.
        CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
        NotificationCompat.Builder mBuilder;
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mBuilder = new NotificationCompat.Builder(
                    this, CHANNEL_ID);
            mNotificationManager.createNotificationChannel(mChannel);

        } else {
            mBuilder = new NotificationCompat.Builder(
                    this, CHANNEL_ID);
        }
        pendingIntent = PendingIntent.getActivity(this, 0,
                backIntent, 0);
        mBuilder.setSmallIcon(R.drawable.logo)
                .setContentTitle(getString(R.string.app_name))
                .setAutoCancel(true)
                .setChannelId(CHANNEL_ID)
                .setContentText(getString(R.string.app_name));
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mNotificationManager.notify(++NOTIFICATION_ID, mBuilder.build());

    }
}
