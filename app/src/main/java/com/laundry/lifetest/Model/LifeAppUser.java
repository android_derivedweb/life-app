package com.laundry.lifetest.Model;

public class LifeAppUser {


    private String ethWalletcurrencyl;
    private String ethWalletaddress;
    private String lifeTokenWalletcurrency;
    private String lifeTokenWalletaddress;
    private String _id;
    private String name;
    private String phone;

    public String getEthWalletcurrencyl() {
        return ethWalletcurrencyl;
    }

    public void setEthWalletcurrencyl(String ethWalletcurrencyl) {
        this.ethWalletcurrencyl = ethWalletcurrencyl;
    }

    public String getEthWalletaddress() {
        return ethWalletaddress;
    }

    public void setEthWalletaddress(String ethWalletaddress) {
        this.ethWalletaddress = ethWalletaddress;
    }

    public String getLifeTokenWalletcurrency() {
        return lifeTokenWalletcurrency;
    }

    public void setLifeTokenWalletcurrency(String lifeTokenWalletcurrency) {
        this.lifeTokenWalletcurrency = lifeTokenWalletcurrency;
    }

    public String getLifeTokenWalletaddress() {
        return lifeTokenWalletaddress;
    }

    public void setLifeTokenWalletaddress(String lifeTokenWalletaddress) {
        this.lifeTokenWalletaddress = lifeTokenWalletaddress;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
