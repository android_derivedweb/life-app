package com.laundry.lifetest.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.laundry.lifetest.Model.TransactionModel;
import com.laundry.lifetest.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/*
 * adapter for home screen slider
 * with image loading
 * */
public class HomeAdp1 extends RecyclerView.Adapter<HomeAdp1.MyViewHolder> {

    /*
     * list and activity delaration
     * */
    ArrayList<TransactionModel> modelArrayList;
    private Activity activity;


    /*
     * view holder class for init View objects from xml layout file
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imv;
        public TextView cyrpto_name;
        public TextView status;
        public TextView usd;
        public TextView t1;

        public MyViewHolder(View view) {
            super(view);
            imv = view.findViewById(R.id.imv);
            cyrpto_name = view.findViewById(R.id.cyrpto_name);
            status = view.findViewById(R.id.status);
            usd = view.findViewById(R.id.usd);
            t1 = view.findViewById(R.id.t1);

        }
    }

    /*
     * constructor for taking data from activity classe
     * */
    public HomeAdp1(Activity activity, ArrayList<TransactionModel> l1) {
        this.modelArrayList = l1;
        this.activity = activity;
    }

    /*
     * connecting layout xml file with java file
     * connecting item layout with adapter
     * */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_item1, parent, false);

        return new MyViewHolder(itemView);
    }
    /*
     *
     * adding click listner to view
     * and setting values from list to view
     * */
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {

            if(modelArrayList.get(position).getTxType().equals("Received")){
                holder.imv.setImageDrawable(activity.getResources().getDrawable(R.drawable.down_arrow1));
            }else {
                holder.imv.setImageDrawable(activity.getResources().getDrawable(R.drawable.up_arrow1));

            }
            holder.cyrpto_name.setText(modelArrayList.get(position).getAmount()+" "+modelArrayList.get(position).getCurrency());
            holder.status.setText(modelArrayList.get(position).getStatus());
            holder.usd.setText(modelArrayList.get(position).getUSD());
            holder.t1.setText(modelArrayList.get(position).getName());

            if(holder.status.getText().equals("Completed")){
                holder.status.setTextColor(activity.getResources().getColor(R.color.green));
            }else {
                holder.status.setTextColor(activity.getResources().getColor(R.color.red));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

}