package com.laundry.lifetest.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.laundry.lifetest.Model.GraphModel;
import com.laundry.lifetest.R;

import java.util.ArrayList;

/*
 * adapter for home screen slider
 * with image loading
 * */
public class HomeAdp extends RecyclerView.Adapter<HomeAdp.MyViewHolder> {

    /*
     * list and activity delaration
     * */
    private ArrayList<GraphModel> modelArrayList;
    private Activity activity;


    /*
     * view holder class for init View objects from xml layout file
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imv;
        public TextView name;
        public TextView price;

        public MyViewHolder(View view) {
            super(view);
            imv = view.findViewById(R.id.imv);
            name = view.findViewById(R.id.name);
            price = view.findViewById(R.id.price);

        }
    }

    /*
     * constructor for taking data from activity classe
     * */
    public HomeAdp(Activity activity, ArrayList<GraphModel> modelArrayList) {
        this.modelArrayList = modelArrayList;
        this.activity = activity;
    }

    /*
     * connecting layout xml file with java file
     * connecting item layout with adapter
     * */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_item, parent, false);

        return new MyViewHolder(itemView);
    }
    /*
     *
     * adding click listner to view
     * and setting values from list to view
     * */
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {

            holder.name.setText(modelArrayList.get(position).getName());
            holder.price.setText(modelArrayList.get(position).getPrice());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

}