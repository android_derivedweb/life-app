package com.laundry.lifetest.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.laundry.lifetest.ItemClick;
import com.laundry.lifetest.Model.LifeAppUser;
import com.laundry.lifetest.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/*
 * adapter for home screen slider
 * with image loading
 * */
public class SendAdp extends RecyclerView.Adapter<SendAdp.MyViewHolder> {

    /*
     * list and activity delaration
     * */
    private ArrayList<LifeAppUser> lifeAppUsers;
    private Activity activity;
    private ItemClick itemClick;


    /*
     * view holder class for init View objects from xml layout file
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imv;
        public RelativeLayout rl1;
        public TextView t2,t1,t3;

        public MyViewHolder(View view) {
            super(view);
            imv = view.findViewById(R.id.imv);
            rl1 = view.findViewById(R.id.rl1);
            t1 = view.findViewById(R.id.t1);
            t2 = view.findViewById(R.id.t2);
            t3 = view.findViewById(R.id.t3);

        }
    }

    /*
     * constructor for taking data from activity classe
     * */
    public SendAdp(Activity activity, ArrayList<LifeAppUser> l1) {
        this.lifeAppUsers = l1;
        this.activity = activity;
        itemClick = (ItemClick) activity;
    }

    /*
     * connecting layout xml file with java file
     * connecting item layout with adapter
     * */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.send_item, parent, false);

        return new MyViewHolder(itemView);
    }
    /*
     *
     * adding click listner to view
     * and setting values from list to view
     * */
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {


            holder.rl1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onClick(holder.getAdapterPosition());
                }
            });

            holder.t1.setText(lifeAppUsers.get(position).getName());
            holder.t3.setText(lifeAppUsers.get(position).getPhone());
            holder.t2.setText(lifeAppUsers.get(position).getLifeTokenWalletaddress());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return lifeAppUsers.size();
    }

}