package com.laundry.lifetest.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.laundry.lifetest.ItemClick;
import com.laundry.lifetest.Model.StakingModel;
import com.laundry.lifetest.R;

import org.json.JSONArray;

import java.util.ArrayList;

/*
 * adapter for home screen slider
 * with image loading
 * */
public class DefiAdp extends RecyclerView.Adapter<DefiAdp.MyViewHolder> {

    /*
     * list and activity delaration
     * */
    private Activity activity;
    private ItemClick itemClick;
    private ArrayList<StakingModel> modelArrayList;
    private final OnItemClickListener listener;
    /*
     * view holder class for init View objects from xml layout file
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public RelativeLayout rl1;
        private TextView t1,t2,t4,t5;

        public MyViewHolder(View view) {
            super(view);
            rl1 = view.findViewById(R.id.rl1);
            t1 = view.findViewById(R.id.t1);
            t2 = view.findViewById(R.id.t2);
            t4 = view.findViewById(R.id.t4);
            t5 = view.findViewById(R.id.t5);

        }
    }

    /*
     * constructor for taking data from activity classe
     * */
    public DefiAdp(Activity activity, ArrayList<StakingModel> modelArrayList,OnItemClickListener listener) {
        this.modelArrayList = modelArrayList;
        this.activity = activity;
        this.listener = listener;
    }

    /*
     * connecting layout xml file with java file
     * connecting item layout with adapter
     * */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.defi_item, parent, false);

        return new MyViewHolder(itemView);
    }
    /*
     *
     * adding click listner to view
     * and setting values from list to view
     * */
    @Override
    public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        try {

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position);
                }
            });
            holder.t1.setText(modelArrayList.get(position).getLockInPeriod());
            holder.t2.setText(modelArrayList.get(position).getCurrency());
            holder.t4.setText(modelArrayList.get(position).getRoi()+"%");
            holder.t5.setText(modelArrayList.get(position).getDuration());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}