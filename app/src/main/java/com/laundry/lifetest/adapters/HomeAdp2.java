package com.laundry.lifetest.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.laundry.lifetest.ItemClick;
import com.laundry.lifetest.R;
import com.laundry.lifetest.dto.HomeDto;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/*
 * adapter for home screen slider
 * with image loading
 * */
public class HomeAdp2 extends RecyclerView.Adapter<HomeAdp2.MyViewHolder> {

    /*
     * list and activity delaration
     * */
    private List<HomeDto> l1;
    private Activity activity;
    private ItemClick itemClick;
    private final OnItemClickListener listener;


    /*
     * view holder class for init View objects from xml layout file
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imv;
        public TextView txt1;
        public RelativeLayout rl1;

        public MyViewHolder(View view) {
            super(view);
            imv = view.findViewById(R.id.imv);
            txt1 = view.findViewById(R.id.txt1);
            rl1 = view.findViewById(R.id.rl1);

        }
    }

    /*
     * constructor for taking data from activity classe
     * */
    public HomeAdp2(Activity activity, List<HomeDto> l1,OnItemClickListener listener) {
        this.l1 = l1;
        this.activity = activity;
        itemClick = (ItemClick) activity;
        this.listener = listener;
    }

    /*
     * connecting layout xml file with java file
     * connecting item layout with adapter
     * */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_item2, parent, false);

        return new MyViewHolder(itemView);
    }
    /*
     *
     * adding click listner to view
     * and setting values from list to view
     * */
    @Override
    public void onBindViewHolder(final MyViewHolder holder, @SuppressLint("RecyclerView") int position) {
        try {
            HomeDto homeDto = l1.get(holder.getAdapterPosition());

            holder.txt1.setText(homeDto.getTitle());
            holder.imv.setImageResource(homeDto.getImg());
            holder.txt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.rl1.performClick();
                }
            });
            holder.imv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.rl1.performClick();
                }
            });

            holder.rl1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position);
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(position);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return l1.size();
    }


    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}