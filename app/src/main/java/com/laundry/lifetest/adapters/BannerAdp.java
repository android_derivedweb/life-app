package com.laundry.lifetest.adapters;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.laundry.lifetest.R;

import org.json.JSONArray;
import org.json.JSONObject;

/*
 * adapter for home screen slider
 * with image loading
 * */
public class BannerAdp extends RecyclerView.Adapter<BannerAdp.MyViewHolder> {

    /*
     * list and activity delaration
     * */
    private int[] l1;
    private Activity activity;


    /*
     * view holder class for init View objects from xml layout file
     * */
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imv;

        public MyViewHolder(View view) {
            super(view);
            imv = view.findViewById(R.id.imv);

        }
    }

    /*
     * constructor for taking data from activity classe
     * */
    public BannerAdp(Activity activity, int[] l1) {
        this.l1 = l1;
        this.activity = activity;
    }

    /*
     * connecting layout xml file with java file
     * connecting item layout with adapter
     * */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.banner_item, parent, false);

        return new MyViewHolder(itemView);
    }
    /*
     *
     * adding click listner to view
     * and setting values from list to view
     * */
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            holder.imv.setImageResource(l1[holder.getAdapterPosition()]);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return l1.length;
    }

}