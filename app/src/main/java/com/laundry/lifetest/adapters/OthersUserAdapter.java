package com.laundry.lifetest.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.laundry.lifetest.Model.OthersUsersModels;
import com.laundry.lifetest.R;

import java.util.ArrayList;

public class OthersUserAdapter extends RecyclerView.Adapter<OthersUserAdapter.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<OthersUsersModels> ModelsArrayList;


    public OthersUserAdapter(Context mContext, ArrayList<OthersUsersModels> othersUsersModels, OnItemClickListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.ModelsArrayList = othersUsersModels;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_others_users, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.aaaaa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
            }
        });


        holder.t1.setText(ModelsArrayList.get(position).getUserName());
        holder.t2.setText(ModelsArrayList.get(position).getAddress());



    }

    @Override
    public int getItemCount() {
        return ModelsArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView t1, t2;
        LinearLayout aaaaa;
        public Viewholder(@NonNull View itemView) {
            super(itemView);

            t1 = itemView.findViewById(R.id.t1);
            t2 = itemView.findViewById(R.id.t2);
            aaaaa = itemView.findViewById(R.id.aaaaa);

        }
    }


    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}