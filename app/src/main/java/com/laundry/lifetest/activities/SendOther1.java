package com.laundry.lifetest.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.AddUserRequest;
import com.laundry.lifetest.API.AllLifeUsersRequest;
import com.laundry.lifetest.API.OthersUsersRequest;
import com.laundry.lifetest.ItemClick;
import com.laundry.lifetest.Model.LifeAppUser;
import com.laundry.lifetest.Model.OthersUsersModels;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;
import com.laundry.lifetest.adapters.HomeAdp;
import com.laundry.lifetest.adapters.HomeAdp1;
import com.laundry.lifetest.adapters.HomeAdp2;
import com.laundry.lifetest.adapters.OthersUserAdapter;
import com.laundry.lifetest.adapters.SendAdp;
import com.laundry.lifetest.dto.HomeDto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SendOther1 extends AppCompatActivity implements ItemClick {

    private  Dialog dialog = null;
    private RecyclerView recListNotification;
    private  ArrayList<OthersUsersModels> othersUsersModels = new ArrayList<>();
    private OthersUserAdapter othersUserAdapter;
    private UserSession userSession;
    private RequestQueue requestQueue;
    private TextView noData;
    private TextView menu_txt;
    private List<HomeDto> l1 = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_oth1);

        findViewById(R.id.addOtherUser).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEditDialog();
            }
        });

        userSession = new UserSession(SendOther1.this);
        requestQueue = Volley.newRequestQueue(SendOther1.this);//Creating the RequestQueue

        noData = findViewById(R.id.noData);

        recListNotification = findViewById(R.id.recListNotification);
        recListNotification.setLayoutManager(new LinearLayoutManager(this));
        othersUserAdapter = new OthersUserAdapter(this,othersUsersModels, new OthersUserAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(getApplicationContext(),SendActivity1.class)
                        .putExtra("id",othersUsersModels.get(item).get_id())
                        .putExtra("name",othersUsersModels.get(item).getUserName())
                        .putExtra("eth_token",othersUsersModels.get(item).getAddress())
                        .putExtra("life_token",othersUsersModels.get(item).getAddress()));
            }
        });
        recListNotification.setAdapter(othersUserAdapter);
        UsersRequest();
    }
    public void MenuNow(View v){

    }

    @Override
    public void onClick(int pos) {

    }

    private void openEditDialog() {
        BottomSheetDialog dialog1 = new BottomSheetDialog(SendOther1.this, R.style.BottomSheetDialog);
        dialog1.setContentView(R.layout.dialog_add_user);


        menu_txt = dialog1.findViewById(R.id.menu_txt);


        EditText userNameET = dialog1.findViewById(R.id.userNameET);
        EditText addressET = dialog1.findViewById(R.id.addressET);
        Button addUser = dialog1.findViewById(R.id.addUser);


        addUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userNameET.getText().toString().isEmpty()){
                    Toast.makeText(dialog1.getContext(), "Enter name", Toast.LENGTH_SHORT).show();
                } else if (addressET.getText().toString().isEmpty()){
                    Toast.makeText(dialog1.getContext(), "Enter address", Toast.LENGTH_SHORT).show();
                } else {

                    AddUser(menu_txt.getText().toString(), userNameET.getText().toString(), addressET.getText().toString());
                    dialog1.dismiss();
                }
            }
        });



        dialog1.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
            }
        });

        dialog1.findViewById(R.id.menu_txt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                try{


                    if(dialog!=null && dialog.isShowing()) {
                        dialog.dismiss();

                    }
                    dialog = new Dialog(dialog1.getContext());
                    dialog.setCancelable(true);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.my_list);
                    dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

                    RecyclerView lv2 = dialog.findViewById(R.id.lv1);

                    l1 = new ArrayList<>();
                    l1.add(new HomeDto("LIFETOKEN",R.drawable.bitcoin));
                    l1.add(new HomeDto("ETH",R.drawable.bitcoin));

                    HomeAdp2 homeAdp1 = new HomeAdp2(dialog1.getOwnerActivity(), l1, new HomeAdp2.OnItemClickListener() {
                        @Override
                        public void onItemClick(int item) {

                            dialog.dismiss();
                            menu_txt.setText(l1.get(item).getTitle());

                        }
                    });

                    LinearLayoutManager layoutManager1 = new LinearLayoutManager(dialog1.getContext(), LinearLayoutManager.VERTICAL, false);
                    lv2.setLayoutManager(layoutManager1);
                    lv2.setItemAnimator(new DefaultItemAnimator());
                    lv2.setNestedScrollingEnabled(false);
                    lv2.setAdapter(homeAdp1);
                    dialog.show();
                }
                catch (Exception e){
                    e.printStackTrace();
                }


            }
        });




        dialog1.show();
    }




    public void NextNow1(View v){
//        startActivity(new Intent(getApplicationContext(),SendOther.class));
//        finish();
    }
    public void NextNow2(View v){
        startActivity(new Intent(getApplicationContext(),SendActivity.class));
        finish();
    }
    public void NextNow(View v){
        try{
            if(dialog!=null && dialog.isShowing()) {
                dialog.dismiss();
            }
            dialog = new Dialog(SendOther1.this);
            dialog.setCancelable(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.sent_pop);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

            dialog.findViewById(R.id.bt1).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(),SendOther.class));
                    finish();
                }
            });

            dialog.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public void FinNow(View v){
        finish();
    }



    private void UsersRequest() {
        final KProgressHUD progressDialog = KProgressHUD.create(SendOther1.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        OthersUsersRequest loginRequest = new OthersUsersRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                othersUsersModels.clear();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONArray resultstransactions = object.getJSONArray("results");
                        //    JSONArray resultstransactions = objectresults.getJSONArray("transactions");

                        for (int i = 0; i < resultstransactions.length(); i++) {
                            JSONObject jsonObject1 = resultstransactions.getJSONObject(i);
                            OthersUsersModels lifeAppUser = new OthersUsersModels();
                            lifeAppUser.set_id(jsonObject1.getString("_id"));
                            lifeAppUser.setCurrency(jsonObject1.getString("currency"));
                            lifeAppUser.setUserName(jsonObject1.getString("userName"));
                            lifeAppUser.setAddress(jsonObject1.getString("address"));
                            othersUsersModels.add(lifeAppUser);
                        }
                        othersUserAdapter.notifyDataSetChanged();

                        if (othersUsersModels.isEmpty()){
                            noData.setVisibility(View.VISIBLE);
                        } else {
                            noData.setVisibility(View.GONE);
                        }
                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(SendOther1.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SendOther1.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(SendOther1.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(SendOther1.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(SendOther1.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }


    private void AddUser(String currency, String userName, String address) {
        final KProgressHUD progressDialog = KProgressHUD.create(SendOther1.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        AddUserRequest loginRequest = new AddUserRequest(currency, userName, address, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getBoolean("success")) {


                        UsersRequest();

                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(SendOther1.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SendOther1.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(SendOther1.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(SendOther1.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(SendOther1.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }

    

}
