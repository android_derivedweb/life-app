package com.laundry.lifetest.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.laundry.lifetest.ItemClick;
import com.laundry.lifetest.R;
import com.laundry.lifetest.adapters.HomeAdp;
import com.laundry.lifetest.adapters.HomeAdp1;
import com.laundry.lifetest.adapters.HomeAdp2;
import com.laundry.lifetest.adapters.SendAdp;
import com.laundry.lifetest.dto.HomeDto;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class SendOther extends AppCompatActivity implements ItemClick {

    private List<HomeDto> l1 = new ArrayList<>();
    private  Dialog dialog = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_oth);
    }
    public void NextNow(View v){
        Intent intent = new Intent(getApplicationContext(),VeryTrans.class);
        intent.putExtra("pos",0);
        startActivity(intent);

        finish();
    }


    public void MenuNow(View v){
        try{
            if(dialog!=null && dialog.isShowing()) {
                dialog.dismiss();

            }
            dialog = new Dialog(SendOther.this);
            dialog.setCancelable(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.my_list);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

            RecyclerView lv2 = dialog.findViewById(R.id.lv1);

            l1 = new ArrayList<>();
            l1.add(new HomeDto("USD (Flat)",R.drawable.doller_ic));
            l1.add(new HomeDto("USD (Flat)",R.drawable.doller_ic));
            l1.add(new HomeDto("USD (Flat)",R.drawable.doller_ic));
            l1.add(new HomeDto("USD (Flat)",R.drawable.doller_ic));
            l1.add(new HomeDto("USD (Flat)",R.drawable.doller_ic));
            l1.add(new HomeDto("USD (Flat)",R.drawable.doller_ic));


            HomeAdp2 homeAdp1 = new HomeAdp2(SendOther.this, l1, new HomeAdp2.OnItemClickListener() {
                @Override
                public void onItemClick(int item) {

                }
            });

            LinearLayoutManager layoutManager1
                    = new LinearLayoutManager(SendOther.this, LinearLayoutManager.VERTICAL, false);
            lv2.setLayoutManager(layoutManager1);
            lv2.setItemAnimator(new DefaultItemAnimator());
            lv2.setNestedScrollingEnabled(false);
            lv2.setAdapter(homeAdp1);
            dialog.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public void FinNow(View v){
        finish();
    }

    @Override
    public void onClick(int pos) {

        ((ImageView)findViewById(R.id.icon1)).setImageResource(l1.get(pos).getImg());
        ((TextView)findViewById(R.id.menu_txt)).setText(l1.get(pos).getTitle());
        if(dialog!=null && dialog.isShowing()) {
            dialog.dismiss();

        }
    }
}
