package com.laundry.lifetest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.LoginRequest;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private UserSession userSession;
    private ImageView close;
    private AppCompatButton btnRegister;
    private EditText mEmail;
    private EditText mPassword;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private RequestQueue requestQueue;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userSession = new UserSession(LoginActivity.this);
        requestQueue = Volley.newRequestQueue(LoginActivity.this);//Creating the RequestQueue


        close = findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),RegActivity.class));
                finish();
            }
        });

        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        findViewById(R.id.login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mEmail.getText().toString().isEmpty()){
                  Toast.makeText(LoginActivity.this, "Please enter your email", Toast.LENGTH_LONG).show();
                }else if (!mEmail.getText().toString().trim().matches(emailPattern)) {
                     Toast.makeText(LoginActivity.this, "Invalid email address", Toast.LENGTH_LONG).show();
                }else if(mPassword.getText().toString().isEmpty()){
                    Toast.makeText(LoginActivity.this, "Please enter your password", Toast.LENGTH_LONG).show();
                }else {
                    LoginRequest(mEmail.getText().toString(),mPassword.getText().toString());
                }
            }
        });

        findViewById(R.id.forgotPassowordText).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPassword.class));

            }
        });

    }

    private void LoginRequest(String email, String password) {
        final KProgressHUD progressDialog = KProgressHUD.create(LoginActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        LoginRequest loginRequest = new LoginRequest(email, password, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONObject objectResult = object.getJSONObject("results");
                        JSONObject objectResultData = objectResult.getJSONObject("data");
                        JSONObject objectResultDataETH = objectResultData.getJSONObject("eth");
                        JSONObject objectResultDataLifeToken = objectResultData.getJSONObject("lifeToken");
                        userSession.createLoginSession(objectResultData.getString("name")
                        ,objectResultData.getString("email")
                        ,objectResultData.getString("phone")
                        ,objectResultDataETH.getString("address")
                        ,objectResultDataETH.getString("balance")
                        ,objectResultDataLifeToken.getString("address")
                        ,objectResultDataLifeToken.getString("balance")
                        ,objectResult.getString("token"));
                        Toast.makeText(LoginActivity.this, object.getString("msg"), Toast.LENGTH_LONG).show();
                        startActivity(new Intent(LoginActivity.this, HomeScreen.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        finish();
                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(LoginActivity.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error.networkResponse.statusCode==406)
                    Toast.makeText(LoginActivity.this, "Password not matched", Toast.LENGTH_LONG).show();
                else if (error.networkResponse.statusCode==404)
                    Toast.makeText(LoginActivity.this, "User not found", Toast.LENGTH_LONG).show();
               else if (error.networkResponse.statusCode==422)
                    Toast.makeText(LoginActivity.this, "Validation error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(LoginActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(LoginActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }



    @Override
    protected void onStart() {
        super.onStart();
        if(userSession.isLoggedIn()){
            startActivity(new Intent(LoginActivity.this,HomeScreen.class));
        }
    }
}
