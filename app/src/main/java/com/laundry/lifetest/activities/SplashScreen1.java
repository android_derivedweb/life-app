package com.laundry.lifetest.activities;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.laundry.lifetest.R;
import com.laundry.lifetest.adapters.BannerAdp;
import com.laundry.lifetest.adapters.ViewPagerAdapter;
import com.laundry.lifetest.fragments.Home;

public class SplashScreen1 extends AppCompatActivity {

    private LinearLayout dots;
    private ViewPager lv1;
    private int[] l1 = {
            R.drawable.splash1,
            R.drawable.splash2,
            R.drawable.splash2
    };

    private int pos = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash1);
        initUI();
        setupVp();
    }

    private void setupVp() {
        setupViewPager(lv1);
        lv1.setOffscreenPageLimit(3);
        addRadio();
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Home(R.drawable.splash1), "1");
        adapter.addFragment(new Home(R.drawable.splash2), "1");
        adapter.addFragment(new Home(R.drawable.splash3), "1");

        viewPager.setAdapter(adapter);
    }

    private void addRadio() {
        try {
            dots.removeAllViews();
            for (int i = 0; i < l1.length; i++) {
                RelativeLayout tabOne = (RelativeLayout)
                        LayoutInflater.from(SplashScreen1.this).inflate(R.layout.dot_layout, null);

                tabOne.setId(i);
                dots.addView(tabOne);

            }
            pos = 0;
            setColor1();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     * changing color of dot
     * */
    private void setColor1() {
        try {
            for (int i = 0; i < l1.length; i++) {
                RelativeLayout rb1 = dots.findViewById(i).findViewById(R.id.rl1);
                if (i == pos) {
                    ViewCompat.setBackgroundTintList(rb1, ColorStateList.valueOf(getResources().getColor(R.color.white1)));
                } else {
                    ViewCompat.setBackgroundTintList(rb1, ColorStateList.valueOf(getResources().getColor(R.color.gry)));
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initUI() {
        dots = findViewById(R.id.dots);
        lv1 = findViewById(R.id.lv1);

        lv1.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                pos = position;
                setColor1();
                if(pos==2){
                    findViewById(R.id.bt1).setVisibility(View.VISIBLE);
                    findViewById(R.id.dots).setVisibility(View.GONE);
                }
                else{
                    findViewById(R.id.bt1).setVisibility(View.GONE);
                    findViewById(R.id.dots).setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void SkipNow(View v){
        startActivity(new Intent(getApplicationContext(),LoginActivity.class));
        finish();
    }
}
