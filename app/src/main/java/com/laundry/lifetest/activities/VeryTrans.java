package com.laundry.lifetest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.laundry.lifetest.R;

public class VeryTrans extends AppCompatActivity {

    private int pos = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vtran);
        pos = getIntent().getExtras().getInt("pos");
    }

    public void NextNow(View v){
        Intent intent = new Intent(getApplicationContext(),VeryTrans1.class);
        intent.putExtra("pos",pos);
        startActivity(intent);
        finish();
    }

    public void FinNow(View v){
        finish();
    }
}
