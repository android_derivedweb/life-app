package com.laundry.lifetest.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.SignUpRequest;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class OtpVerification extends AppCompatActivity {


    private FirebaseAuth mAuth;

    private String verificationId, phone, userName, userEmail, userPass;

    private EditText edit1, edit2, edit3, edit4, edit5, edit6;


    private UserSession userSession;
    private RequestQueue requestQueue;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        userSession = new UserSession(OtpVerification.this);
        requestQueue = Volley.newRequestQueue(OtpVerification.this);//Creating the RequestQueue

        mAuth = FirebaseAuth.getInstance();


        edit1 = findViewById(R.id.edit1);
        edit2 = findViewById(R.id.edit2);
        edit3 = findViewById(R.id.edit3);
        edit4 = findViewById(R.id.edit4);
        edit5 = findViewById(R.id.edit5);
        edit6 = findViewById(R.id.edit6);


        edit1.addTextChangedListener(new GenericTextWatcher(edit1));
        edit2.addTextChangedListener(new GenericTextWatcher(edit2));
        edit3.addTextChangedListener(new GenericTextWatcher(edit3));
        edit4.addTextChangedListener(new GenericTextWatcher(edit4));
        edit5.addTextChangedListener(new GenericTextWatcher(edit5));
        edit6.addTextChangedListener(new GenericTextWatcher(edit6));



        findViewById(R.id.btnVeriftOtp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edit1.getText().toString().isEmpty() || edit2.getText().toString().isEmpty() ||
                        edit3.getText().toString().isEmpty() || edit4.getText().toString().isEmpty() ||
                        edit5.getText().toString().isEmpty() || edit6.getText().toString().isEmpty()){

                    Toast.makeText(OtpVerification.this, "Enter OTP!", Toast.LENGTH_SHORT).show();

                } else {
                    verifyCode(edit1.getText().toString() + edit2.getText().toString() + edit3.getText().toString() + edit4.getText().toString() +
                            edit5.getText().toString() + edit6.getText().toString());
                }


            }
        });

        phone = getIntent().getStringExtra("phoneForOtp");
        userName = getIntent().getStringExtra("userName");
        userEmail = getIntent().getStringExtra("userEmail");
        userPass = getIntent().getStringExtra("userPass");


    //    Log.e("checkPhone", phone);

        sendVerificationCode(phone);


    }


    private void sendVerificationCode(String number) {
        // this method is used for getting
        // OTP on user phone number.
        PhoneAuthOptions options =
                PhoneAuthOptions.newBuilder(mAuth)
                        .setPhoneNumber(number)            // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(this)                 // Activity (for callback binding)
                        .setCallbacks(mCallBack)           // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }


    // callback method is called on Phone auth provider.
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks

            // initializing our callbacks for on
            // verification callback method.
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        // below method is used when
        // OTP is sent from Firebase
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            // when we receive the OTP it
            // contains a unique id which
            // we are storing in our string
            // which we have already created.
            verificationId = s;
        }

        // this method is called when user
        // receive OTP from Firebase.
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            // below line is used for getting OTP code
            // which is sent in phone auth credentials.
            final String code = phoneAuthCredential.getSmsCode();

            // checking if the code
            // is null or not.
            if (code != null) {
                // if the code is not null then
                // we are setting that code to
                // our OTP edittext field.
                edit1.setText(code.substring(0,1));
                edit2.setText(code.substring(1,2));
                edit3.setText(code.substring(2,3));
                edit4.setText(code.substring(3,4));
                edit5.setText(code.substring(4,5));
                edit6.setText(code.substring(5,6));

                // after setting this code
                // to OTP edittext field we
                // are calling our verifycode method.

            }
        }

        // this method is called when firebase doesn't
        // sends our OTP code due to any error or issue.
        @Override
        public void onVerificationFailed(FirebaseException e) {
            // displaying error message with firebase exception.
            Toast.makeText(OtpVerification.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    };


    // below method is use to verify code from Firebase.
    private void verifyCode(String code) {
        // below line is used for getting getting
        // credentials from our verification id and code.
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);

        // after getting credential we are
        // calling sign in method.
        signInWithCredential(credential);
    }



    private void signInWithCredential(PhoneAuthCredential credential) {
        // inside this method we are checking if
        // the code entered is correct or not.
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // if the code is correct and the task is successful
                            // we are sending our user to new activity.

                            LoginRequest(userName, userEmail, phone, userPass);

                        } else {
                            // if the code is not correct then we are
                            // displaying an error message to the user.
                            Toast.makeText(OtpVerification.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }



    public void NextNow(View v){
        startActivity(new Intent(getApplicationContext(),HomeScreen.class));
        finish();
    }



    private void LoginRequest(String name, String email,String phone , String password) {


        final KProgressHUD progressDialog = KProgressHUD.create(OtpVerification.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        SignUpRequest loginRequest = new SignUpRequest(name,email, phone,password, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getBoolean("success")) {

                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONObject objectResult = object.getJSONObject("results");
                        JSONObject objectResultData = objectResult.getJSONObject("data");

                        userSession.createLoginSession(objectResultData.getString("name")
                                ,objectResultData.getString("email")
                                ,objectResultData.getString("phone")
                                ,objectResultData.getString("ethAddress")
                                ,objectResultData.getString("ethBalance")
                                ,objectResultData.getString("lifeTokenAddress")
                                ,objectResultData.getString("lifeTokenBalance")
                                ,objectResult.getString("token"));

                        Toast.makeText(OtpVerification.this, object.getString("msg"), Toast.LENGTH_LONG).show();



                        startActivity(new Intent(OtpVerification.this, HomeScreen.class)
                                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                        finish();


                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(OtpVerification.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(OtpVerification.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error.networkResponse.statusCode==406)
                    Toast.makeText(OtpVerification.this, "Password not matched", Toast.LENGTH_LONG).show();
                else if (error.networkResponse.statusCode==404)
                    Toast.makeText(OtpVerification.this, "User not found", Toast.LENGTH_LONG).show();
                else if (error.networkResponse.statusCode==422)
                    Toast.makeText(OtpVerification.this, "Validation error", Toast.LENGTH_LONG).show();
                else if (error instanceof ServerError)
                    Toast.makeText(OtpVerification.this, "Please use diffrent email or number", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(OtpVerification.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(OtpVerification.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        loginRequest.setRetryPolicy(new DefaultRetryPolicy(
                180000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(loginRequest);
    }


















    public void FinNow(View v){
        finish();
    }


    public class GenericTextWatcher implements TextWatcher
    {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @SuppressLint("NonConstantResourceId")
        @Override
        public void afterTextChanged(Editable editable) {
// TODO Auto-generated method stub
            String text = editable.toString();
            switch(view.getId())
            {

                case R.id.edit1:
                    if(text.length()==1)
                        edit2.requestFocus();
                    break;
                case R.id.edit2:
                    if(text.length()==1)
                        edit3.requestFocus();
                    else if(text.length()==0)
                        edit1.requestFocus();
                    break;
                case R.id.edit3:
                    if(text.length()==1)
                        edit4.requestFocus();
                    else if(text.length()==0)
                        edit2.requestFocus();
                    break;
                case R.id.edit4:
                    if(text.length()==1)
                        edit5.requestFocus();
                    else if(text.length()==0)
                        edit3.requestFocus();
                    break;
                case R.id.edit5:
                    if(text.length()==1)
                        edit6.requestFocus();
                    else if(text.length()==0)
                        edit4.requestFocus();
                    break;
                case R.id.edit6:
                    if(text.length()==0) {
                        edit5.requestFocus();
                    }else {
                        View view = getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
// TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
// TODO Auto-generated method stub
        }
    }



}
