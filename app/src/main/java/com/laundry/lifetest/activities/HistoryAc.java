package com.laundry.lifetest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.StakingRequest;
import com.laundry.lifetest.Model.StakingModel;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;
import com.laundry.lifetest.adapters.DefiAdp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class HistoryAc extends AppCompatActivity {

    private RecyclerView lv1;
    private ArrayList<StakingModel> modelArrayList = new ArrayList<StakingModel>();
    private DefiAdp homeAdp1;
    private UserSession userSession;
    private RequestQueue requestQueue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_his);
        userSession = new UserSession(HistoryAc.this);
        requestQueue = Volley.newRequestQueue(HistoryAc.this);//Creating the RequestQueue


        
        initUI();
        addAdp();
        StakingRequest();
    }

    private void addAdp() {



        homeAdp1 = new DefiAdp(HistoryAc.this, modelArrayList, new DefiAdp.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                startActivity(new Intent(getApplicationContext(),DefiAc1.class).putExtra("StakId",modelArrayList.get(item).get_id()));

            }
        });

        LinearLayoutManager layoutManager1
                = new LinearLayoutManager(HistoryAc.this, LinearLayoutManager.VERTICAL, false);
        lv1.setLayoutManager(layoutManager1);
        lv1.setItemAnimator(new DefaultItemAnimator());
        lv1.setNestedScrollingEnabled(false);
        lv1.setAdapter(homeAdp1);
    }
    private void initUI() {
        lv1 = findViewById(R.id.lv1);
    }


    public void FinNow(View v){
        finish();
    }


    private void StakingRequest() {
        final KProgressHUD progressDialog = KProgressHUD.create(HistoryAc.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        StakingRequest loginRequest = new StakingRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONArray resultstransactions = object.getJSONArray("results");
                        //    JSONArray resultstransactions = objectresults.getJSONArray("transactions");

                        ArrayList<Integer> dataInt = new ArrayList<>();
                        ArrayList<StakingModel> ascendingArrayList = new ArrayList<>();



                        for (int i = 0; i < resultstransactions.length(); i++) {
                            JSONObject jsonObject1 = resultstransactions.getJSONObject(i);
                            StakingModel stakingModel = new StakingModel();
                            stakingModel.setLockInPeriod(jsonObject1.getString("lockInPeriod"));
                            stakingModel.set_id(jsonObject1.getString("_id"));
                            stakingModel.setStatus(jsonObject1.getString("status"));
                            stakingModel.setCurrency(jsonObject1.getString("currency"));
                            stakingModel.setAmount(jsonObject1.getString("amount"));
                            stakingModel.setStartDate(jsonObject1.getString("startDate"));
                            stakingModel.setEndDate(jsonObject1.getString("endDate"));
                            stakingModel.setDuration(jsonObject1.getString("duration"));
                            stakingModel.setRoi(jsonObject1.getString("roi"));
                            stakingModel.setCreatedAt(jsonObject1.getString("createdAt"));
                            stakingModel.setDaysPassed(jsonObject1.getString("daysPassed"));

                            dataInt.add(Integer.parseInt(jsonObject1.getString("roi")));
                            modelArrayList.add(stakingModel);

                        }
                        homeAdp1.notifyDataSetChanged();


                        TreeMap<Integer,Integer> m1 = new TreeMap<>();
                        for(int i : dataInt)
                        {
                            if(m1.containsKey(i))
                            {m1.put(i,m1.get(i)+1);}
                            else  {m1.put(i,1);}

                            Log.e("m1", m1.toString() +"");
                        };

                        ArrayList<Integer> b = new ArrayList<>();
                        m1.entrySet().stream().sorted((k1, k2) -> -k1.getValue().compareTo(k2.getValue())).forEach(e -> {
                            for(int i=0;i<e.getValue();i++) {
                                b.add(e.getKey());

                            }
                        });


                        Log.e("checkAscending", ascendingArrayList.toString() +"");


                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(HistoryAc.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(HistoryAc.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(HistoryAc.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(HistoryAc.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(HistoryAc.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }

    

}
