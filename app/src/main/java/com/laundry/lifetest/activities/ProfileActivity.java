package com.laundry.lifetest.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.GetProfile;
import com.laundry.lifetest.API.SetNewPasswordRequest2;
import com.laundry.lifetest.API.UpdateProfile;
import com.laundry.lifetest.API.WalletRequest;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {


    private UserSession userSession;
    private RequestQueue requestQueue;

    private EditText nameET;
    private TextView walletBal, ethBal, ethUSD, lifeTokenBal, lifeTokenUSD, stakingUSD, stakingBal;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        nameET = findViewById(R.id.nameET);
        walletBal = findViewById(R.id.walletBal);
        ethBal = findViewById(R.id.ethBal);
        ethUSD = findViewById(R.id.ethUSD);
        lifeTokenBal = findViewById(R.id.lifeTokenBal);
        lifeTokenUSD = findViewById(R.id.lifeTokenUSD);
        stakingUSD = findViewById(R.id.stakingUSD);
        stakingBal = findViewById(R.id.stakingBal);



        userSession = new UserSession(ProfileActivity.this);
        requestQueue = Volley.newRequestQueue(ProfileActivity.this);//Creating the RequestQueue



        findViewById(R.id.updateName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nameET.getText().toString().isEmpty()){
                    Toast.makeText(ProfileActivity.this, "Name can't be empty!", Toast.LENGTH_SHORT).show();
                } else {
                    updateProfile();
                }
            }
        });


        getProfile();

    }


    private void updateProfile() {
        final KProgressHUD progressDialog = KProgressHUD.create(ProfileActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        UpdateProfile loginRequest = new UpdateProfile(nameET.getText().toString(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");

                        Toast.makeText(ProfileActivity.this, object.getString("msg"), Toast.LENGTH_LONG).show();


                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(ProfileActivity.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(ProfileActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(ProfileActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(ProfileActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }





    private void getProfile() {
        final KProgressHUD progressDialog = KProgressHUD.create(ProfileActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        GetProfile loginRequest = new GetProfile(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONObject objectresults = object.getJSONObject("results");
                        JSONObject objectresults1 = objectresults.getJSONObject("eth");
                        JSONObject objectresults2 = objectresults.getJSONObject("lifeToken");
                        JSONObject objectresults3 = objectresults.getJSONObject("staking");

                        nameET.setText(objectresults.getString("name"));

                        walletBal.setText(objectresults.getString("total") + " USD");

                        ethBal.setText("Balance : " + objectresults1.getString("balance"));
                        ethUSD.setText(objectresults1.getString("usd"));

                        lifeTokenBal.setText("Balance : " + objectresults2.getString("balance"));
                        lifeTokenUSD.setText(objectresults2.getString("usd"));

                        stakingBal.setText("Balance : " + objectresults3.getString("balance"));
                        stakingUSD.setText(objectresults3.getString("usd"));



                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(ProfileActivity.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ProfileActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(ProfileActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(ProfileActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(ProfileActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }




    public void NextNow(View v){
        finish();
    }


    public void FinNow(View v){
        finish();
    }


}
