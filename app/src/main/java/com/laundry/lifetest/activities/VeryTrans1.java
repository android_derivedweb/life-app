package com.laundry.lifetest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.laundry.lifetest.R;

public class VeryTrans1 extends AppCompatActivity {

    private int pos = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vtran1);
        pos = getIntent().getExtras().getInt("pos");
    }

    public void NextNow(View v){
        if(pos==0) {
            startActivity(new Intent(getApplicationContext(), AirDropAc1.class));
        }
        else{
            startActivity(new Intent(getApplicationContext(), VerifyTrans2.class));
        }
        finish();
    }

    public void FinNow(View v){
        finish();
    }
}
