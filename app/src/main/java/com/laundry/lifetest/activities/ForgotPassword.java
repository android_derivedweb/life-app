package com.laundry.lifetest.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthOptions;
import com.google.firebase.auth.PhoneAuthProvider;
import com.laundry.lifetest.Const;
import com.laundry.lifetest.R;
import com.laundry.lifetest.adapters.AutoAdp;
import com.laundry.lifetest.dto.HomeDto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ForgotPassword extends AppCompatActivity {

    private  Dialog dialog = null;
    private AutoAdp sa;
    private JSONArray countries = null, countries1 = null;
    private TextView cc_digit;
    private Button done;
    private Button checkOTP;
    private EditText edit1, edit2, edit3, edit4, edit5, edit6,number;
    private FirebaseAuth mAuth;
    private String verificationId;
    private LinearLayout password;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        cc_digit = findViewById(R.id.cc_digit);
        number = findViewById(R.id.number);
        password = findViewById(R.id.password);
        done = findViewById(R.id.done);
        checkOTP = findViewById(R.id.checkOTP);
        try {
            countries = new JSONArray(Const.country_codes);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mAuth = FirebaseAuth.getInstance();

        edit1 = findViewById(R.id.edit1);
        edit2 = findViewById(R.id.edit2);
        edit3 = findViewById(R.id.edit3);
        edit4 = findViewById(R.id.edit4);
        edit5 = findViewById(R.id.edit5);
        edit6 = findViewById(R.id.edit6);

        edit1.addTextChangedListener(new GenericTextWatcher(edit1));
        edit2.addTextChangedListener(new GenericTextWatcher(edit2));
        edit3.addTextChangedListener(new GenericTextWatcher(edit3));
        edit4.addTextChangedListener(new GenericTextWatcher(edit4));
        edit5.addTextChangedListener(new GenericTextWatcher(edit5));
        edit6.addTextChangedListener(new GenericTextWatcher(edit6));


        


        cc_digit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConNow();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (number.getText().toString().isEmpty()) {
                    Toast.makeText(ForgotPassword.this, "Please enter phone number!", Toast.LENGTH_SHORT).show();
                } else {
                    password.setVisibility(View.VISIBLE);
                    done.setVisibility(View.GONE);
                    checkOTP.setVisibility(View.VISIBLE);
                    sendVerificationCode(cc_digit.getText() + number.getText().toString());
                }



            }
        });

        checkOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (edit1.getText().toString().isEmpty() || edit2.getText().toString().isEmpty() ||
                        edit3.getText().toString().isEmpty() || edit4.getText().toString().isEmpty() ||
                        edit5.getText().toString().isEmpty() || edit6.getText().toString().isEmpty()) {

                    Toast.makeText(ForgotPassword.this, "Enter OTP!", Toast.LENGTH_SHORT).show();

                } else {
                    verifyCode(edit1.getText().toString() + edit2.getText().toString() + edit3.getText().toString() + edit4.getText().toString() +
                            edit5.getText().toString() + edit6.getText().toString());
                }
            }
        });

    }
    public void NextNow(View v){
        finish();
    }

    public void ConNow() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
                dialog = null;
            }

            dialog = new Dialog(this);
            dialog.setCancelable(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            Rect displayRectangle = new Rect();
            Window window = dialog.getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
            window.setBackgroundDrawableResource(android.R.color.transparent);


            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.country_code_popup, null);
            layout.setMinimumWidth((int)(displayRectangle.width() * 0.9f));
            layout.setMinimumHeight((int)(displayRectangle.height() * 0.9f));
            dialog.setContentView(layout);

            final ListView gv1 = dialog.findViewById(R.id.gv1);
            final EditText search_et = dialog.findViewById(R.id.search_et);


            sa = new AutoAdp(this, countries);
            gv1.setAdapter(sa);
            gv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        JSONObject country = countries.getJSONObject(position);
                        cc_digit.setText(country.getString(Const.dial_code));
//                        cname = country.getString("name");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
            });
            search_et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (search_et.getText().toString().length() > 0) {
                        countries1 = filterData(search_et.getText().toString());
                        sa = new AutoAdp(ForgotPassword.this, countries1);
                        gv1.setAdapter(sa);
                        gv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                try {
                                    JSONObject country = countries1.getJSONObject(position);
                                    cc_digit.setText(country.getString(Const.dial_code));
//                                    cname = country.getString("name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                dialog.dismiss();
                            }
                        });
                    } else {
                        sa = new AutoAdp(ForgotPassword.this, countries);
                        gv1.setAdapter(sa);
                        gv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                try {
                                    JSONObject country = countries.getJSONObject(position);
                                    cc_digit.setText(country.getString(Const.dial_code));
//                                    cname = country.getString("name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                dialog.dismiss();
                            }
                        });
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JSONArray filterData(String s) {
        JSONArray lk = new JSONArray();
        try {
            for (int i = 0; i < countries.length(); i++) {
                if (countries.getJSONObject(i).getString(Const.name).toLowerCase().toLowerCase().contains(s.toLowerCase().trim())) {
                    lk.put(countries.getJSONObject(i));
                }
                else if (countries.getJSONObject(i).getString(Const.dial_code).toLowerCase().toLowerCase().contains(s.toLowerCase().trim())) {
                    lk.put(countries.getJSONObject(i));
                }
                else if (countries.getJSONObject(i).getString(Const.code).toLowerCase().toLowerCase().contains(s.toLowerCase().trim())) {
                    lk.put(countries.getJSONObject(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lk;
    }

    public void FinNow(View v){
        finish();
    }

    private void sendVerificationCode(String number) {
        // this method is used for getting
        // OTP on user phone number.
        PhoneAuthOptions options = PhoneAuthOptions.newBuilder(mAuth)
                        .setPhoneNumber(number)            // Phone number to verify
                        .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                        .setActivity(this)                 // Activity (for callback binding)
                        .setCallbacks(mCallBack)           // OnVerificationStateChangedCallbacks
                        .build();
        PhoneAuthProvider.verifyPhoneNumber(options);
    }


    // callback method is called on Phone auth provider.
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks

            // initializing our callbacks for on
            // verification callback method.

            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        // below method is used when
        // OTP is sent from Firebase
        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            // when we receive the OTP it
            // contains a unique id which
            // we are storing in our string
            // which we have already created.
            verificationId = s;
        }

        // this method is called when user
        // receive OTP from Firebase.
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            // below line is used for getting OTP code
            // which is sent in phone auth credentials.
            final String code = phoneAuthCredential.getSmsCode();

            // checking if the code
            // is null or not.
            if (code != null) {
                // if the code is not null then
                // we are setting that code to
                // our OTP edittext field.
                edit1.setText(code.substring(0,1));
                edit2.setText(code.substring(1,2));
                edit3.setText(code.substring(2,3));
                edit4.setText(code.substring(3,4));
                edit5.setText(code.substring(4,5));
                edit6.setText(code.substring(5,6));

                // after setting this code
                // to OTP edittext field we
                // are calling our verifycode method.

            }
        }

        // this method is called when firebase doesn't
        // sends our OTP code due to any error or issue.
        @Override
        public void onVerificationFailed(FirebaseException e) {
            // displaying error message with firebase exception.
            Toast.makeText(ForgotPassword.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    };


    // below method is use to verify code from Firebase.
    private void verifyCode(String code) {
        // below line is used for getting getting
        // credentials from our verification id and code.
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);

        // after getting credential we are
        // calling sign in method.
        signInWithCredential(credential);
    }



    private void signInWithCredential(PhoneAuthCredential credential) {
        // inside this method we are checking if
        // the code entered is correct or not.
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // if the code is correct and the task is successful
                            // we are sending our user to new activity.

                            //LoginRequest(userName, userEmail, phone, userPass);
                            startActivity(new Intent(ForgotPassword.this,PasswordSetActivity.class)
                            .putExtra("mobileNumber", cc_digit.getText().toString() + number.getText().toString()));
                            finish();

                        } else {
                            // if the code is not correct then we are
                            // displaying an error message to the user.
                            Toast.makeText(ForgotPassword.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


    public class GenericTextWatcher implements TextWatcher
    {
        private View view;
        private GenericTextWatcher(View view)
        {
            this.view = view;
        }

        @SuppressLint("NonConstantResourceId")
        @Override
        public void afterTextChanged(Editable editable) {
// TODO Auto-generated method stub
            String text = editable.toString();
            switch(view.getId())
            {

                case R.id.edit1:
                    if(text.length()==1)
                        edit2.requestFocus();
                    break;
                case R.id.edit2:
                    if(text.length()==1)
                        edit3.requestFocus();
                    else if(text.length()==0)
                        edit1.requestFocus();
                    break;
                case R.id.edit3:
                    if(text.length()==1)
                        edit4.requestFocus();
                    else if(text.length()==0)
                        edit2.requestFocus();
                    break;
                case R.id.edit4:
                    if(text.length()==1)
                        edit5.requestFocus();
                    else if(text.length()==0)
                        edit3.requestFocus();
                    break;
                case R.id.edit5:
                    if(text.length()==1)
                        edit6.requestFocus();
                    else if(text.length()==0)
                        edit4.requestFocus();
                    break;
                case R.id.edit6:
                    if(text.length()==0) {
                        edit5.requestFocus();
                    }else {
                        View view = getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    }
                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
// TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
// TODO Auto-generated method stub
        }
    }

    

}
