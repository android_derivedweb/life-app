package com.laundry.lifetest.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.LoginRequest;
import com.laundry.lifetest.API.SignUpRequest;
import com.laundry.lifetest.Const;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;
import com.laundry.lifetest.adapters.AutoAdp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegActivity extends AppCompatActivity {

    private AutoAdp sa;
    private TextView cc_digit;
    private JSONArray countries = null, countries1 = null;
    private EditText mUserName;
    private EditText mEmail;
    private EditText mPhone;
    private EditText mPassword;
    private CheckBox agree_txt;
    private boolean isPrivacyChecked = false;
    private Button mSubmit;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private UserSession userSession;
    private RequestQueue requestQueue;


    private JSONArray filterData(String s) {
        JSONArray lk = new JSONArray();
        try {
            for (int i = 0; i < countries.length(); i++) {
                if (countries.getJSONObject(i).getString(Const.name).toLowerCase().toLowerCase().contains(s.toLowerCase().trim())) {
                    lk.put(countries.getJSONObject(i));
                }
                else if (countries.getJSONObject(i).getString(Const.dial_code).toLowerCase().toLowerCase().contains(s.toLowerCase().trim())) {
                    lk.put(countries.getJSONObject(i));
                }
                else if (countries.getJSONObject(i).getString(Const.code).toLowerCase().toLowerCase().contains(s.toLowerCase().trim())) {
                    lk.put(countries.getJSONObject(i));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lk;
    }
    private Dialog dialog = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        userSession = new UserSession(RegActivity.this);
        requestQueue = Volley.newRequestQueue(RegActivity.this);//Creating the RequestQueue


        
        
        cc_digit = findViewById(R.id.cc_digit);
        try {
            countries = new JSONArray(Const.country_codes);

        } catch (Exception e) {
            e.printStackTrace();
        }

        mUserName = findViewById(R.id.name);
        mEmail = findViewById(R.id.email);
        mPhone = findViewById(R.id.number);
        mPassword = findViewById(R.id.passowrd);
        agree_txt = findViewById(R.id.agree_txt);
        mSubmit = findViewById(R.id.submit);

        agree_txt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isPrivacyChecked = isChecked;

                Log.e("isPrivacyChecked",isPrivacyChecked+"");
            }
        });
        mSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("isPrivacyChecked",isPrivacyChecked+"");
                if(mUserName.getText().toString().isEmpty()){
                    Toast.makeText(RegActivity.this, "Please enter name", Toast.LENGTH_LONG).show();
                }else if(mEmail.getText().toString().isEmpty()){
                    Toast.makeText(RegActivity.this, "Please enter your email", Toast.LENGTH_LONG).show();
                }else if (!mEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(RegActivity.this, "Invalid email address", Toast.LENGTH_LONG).show();
                }else if(mPhone.getText().toString().isEmpty()){
                    Toast.makeText(RegActivity.this, "Please enter number", Toast.LENGTH_LONG).show();
                }else if(mPassword.getText().toString().isEmpty()){
                    Toast.makeText(RegActivity.this, "Please enter your password", Toast.LENGTH_LONG).show();
                }else if(!isPrivacyChecked){
                    Toast.makeText(RegActivity.this, "Please agree with our terms", Toast.LENGTH_LONG).show();

                }else {

                    Log.e("checkData", mUserName.getText().toString() +"--" + mEmail.getText().toString() +"--" +
                            cc_digit.getText().toString() + mPhone.getText().toString() + "--" +
                            mPassword.getText().toString());

                    String phone = cc_digit.getText().toString() + mPhone.getText().toString();

                    startActivity(new Intent(RegActivity.this, OtpVerification.class)
                            .putExtra("phoneForOtp", phone)
                            .putExtra("userName", mUserName.getText().toString())
                            .putExtra("userEmail", mEmail.getText().toString())
                            .putExtra("userPass", mPassword.getText().toString()));


                //    LoginRequest(mUserName.getText().toString(),mEmail.getText().toString(),cc_digit.getText().toString()+mPhone.getText().toString(),mPassword.getText().toString());
                }
            }
            
        });
        
    }


    private void LoginRequest(String name, String email,String phone , String password) {


        final KProgressHUD progressDialog = KProgressHUD.create(RegActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        SignUpRequest loginRequest = new SignUpRequest(name,email, phone,password, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if (jsonObject.getBoolean("success")) {

                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONObject objectResult = object.getJSONObject("results");
                        JSONObject objectResultData = objectResult.getJSONObject("data");

                        userSession.createLoginSession(objectResultData.getString("name")
                                ,objectResultData.getString("email")
                                ,objectResultData.getString("phone")
                                ,objectResultData.getString("ethAddress")
                                ,objectResultData.getString("ethBalance")
                                ,objectResultData.getString("lifeTokenAddress")
                                ,objectResultData.getString("lifeTokenBalance")
                                ,objectResult.getString("token"));

                        Toast.makeText(RegActivity.this, object.getString("msg"), Toast.LENGTH_LONG).show();



                        if (TextUtils.isEmpty(mPhone.getText().toString())) {
                            // when mobile number text field is empty
                            // displaying a toast message.
                            Toast.makeText(RegActivity.this, "Please enter a valid phone number.", Toast.LENGTH_SHORT).show();
                        } else {
                            // if the text field is not empty we are calling our
                            // send OTP method for getting OTP from Firebase.
                            String phone = cc_digit.getText().toString() + mPhone.getText().toString();

                            startActivity(new Intent(RegActivity.this, OtpVerification.class)
                                    .putExtra("phoneForOtp", phone)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

                            finish();
                        }


                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(RegActivity.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(RegActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error.networkResponse.statusCode==406)
                    Toast.makeText(RegActivity.this, "Password not matched", Toast.LENGTH_LONG).show();
                else if (error.networkResponse.statusCode==404)
                    Toast.makeText(RegActivity.this, "User not found", Toast.LENGTH_LONG).show();
                else if (error.networkResponse.statusCode==422)
                    Toast.makeText(RegActivity.this, "Validation error", Toast.LENGTH_LONG).show();
               else if (error instanceof ServerError)
                    Toast.makeText(RegActivity.this, "Please use diffrent email or number", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(RegActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(RegActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                //params.put("Authorization","Bearer "+ Session.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        loginRequest.setRetryPolicy(new DefaultRetryPolicy(
                180000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(loginRequest);
    }





    public void ConNow(View v) {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
                dialog = null;
            }

            dialog = new Dialog(this);
            dialog.setCancelable(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

            Rect displayRectangle = new Rect();
            Window window = dialog.getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
            window.setBackgroundDrawableResource(android.R.color.transparent);


            LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.country_code_popup, null);
            layout.setMinimumWidth((int)(displayRectangle.width() * 0.9f));
            layout.setMinimumHeight((int)(displayRectangle.height() * 0.9f));
            dialog.setContentView(layout);

            final ListView gv1 = dialog.findViewById(R.id.gv1);
            final EditText search_et = dialog.findViewById(R.id.search_et);


            sa = new AutoAdp(this, countries);
            gv1.setAdapter(sa);
            gv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    try {
                        JSONObject country = countries.getJSONObject(position);
                        cc_digit.setText(country.getString(Const.dial_code));
//                        cname = country.getString("name");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();
                }
            });
            search_et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (search_et.getText().toString().length() > 0) {
                        countries1 = filterData(search_et.getText().toString());
                        sa = new AutoAdp(RegActivity.this, countries1);
                        gv1.setAdapter(sa);
                        gv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                try {
                                    JSONObject country = countries1.getJSONObject(position);
                                    cc_digit.setText(country.getString(Const.dial_code));
//                                    cname = country.getString("name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                dialog.dismiss();
                            }
                        });
                    } else {
                        sa = new AutoAdp(RegActivity.this, countries);
                        gv1.setAdapter(sa);
                        gv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                try {
                                    JSONObject country = countries.getJSONObject(position);
                                    cc_digit.setText(country.getString(Const.dial_code));
//                                    cname = country.getString("name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                dialog.dismiss();
                            }
                        });
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });


            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void FinNow(View v){
        finish();
    }
}
