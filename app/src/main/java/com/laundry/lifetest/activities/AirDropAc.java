package com.laundry.lifetest.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.laundry.lifetest.ItemClick;
import com.laundry.lifetest.R;
import com.laundry.lifetest.adapters.HomeAdp;
import com.laundry.lifetest.adapters.HomeAdp1;
import com.laundry.lifetest.adapters.HomeAdp2;
import com.laundry.lifetest.adapters.SendAdp;
import com.laundry.lifetest.dto.HomeDto;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class AirDropAc extends AppCompatActivity {

    private List<HomeDto> l1 = new ArrayList<>();
    private  Dialog dialog = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_air_drop);
    }
    public void NextNow(View v){
        startActivity(new Intent(getApplicationContext(),AirDropAc1.class));
        finish();
    }



    public void FinNow(View v){
        finish();
    }

}
