package com.laundry.lifetest.activities;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.WalletRequest;
import com.laundry.lifetest.ItemClick;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;
import com.laundry.lifetest.adapters.HomeAdp;
import com.laundry.lifetest.adapters.HomeAdp1;
import com.laundry.lifetest.adapters.HomeAdp2;
import com.laundry.lifetest.adapters.SendAdp;
import com.laundry.lifetest.dto.HomeDto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SendActivity1 extends AppCompatActivity implements ItemClick {

    private List<HomeDto> l1 = new ArrayList<>();
    private  Dialog dialog = null;
    private String name,eth_token,life_token,id;
    private TextView t1,t2,t4,t42;
    private UserSession userSession;
    private RequestQueue requestQueue;
    private String total = "";
    private String COINSTATUS = "LIFETOKEN";
    private EditText et1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send1);


        userSession = new UserSession(SendActivity1.this);
        requestQueue = Volley.newRequestQueue(SendActivity1.this);//Creating the RequestQueue

        
        name = getIntent().getStringExtra("name");
        eth_token = getIntent().getStringExtra("eth_token");
        life_token = getIntent().getStringExtra("life_token");
        id = getIntent().getStringExtra("id");

        t1 = findViewById(R.id.t1);
        t2 = findViewById(R.id.t2);
        t4 = findViewById(R.id.t4);
        et1 = findViewById(R.id.et1);

        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()>0){
                    try {
                        float v = Float.parseFloat(total);
                        float f = Float.parseFloat(s.toString());
                        if(f > v){
                            Toast.makeText(SendActivity1.this,"you can't enter amount greater than your balance!!!",Toast.LENGTH_LONG).show();
                            et1.setText("");
                        }
                    }catch (Exception e){
                       Log.e("Exception",e.getMessage()) ;
                       Toast.makeText(SendActivity1.this,"Please enter number only!!!",Toast.LENGTH_LONG).show();
                        et1.setText("");
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        t1.setText(name);
        t2.setText(eth_token);

        WalletRequest("LIFETOKEN");

    }

    public void NextNow(View v){

        if(!et1.getText().toString().isEmpty() || !et1.getText().toString().equals("0") ) {
            startActivity(new Intent(getApplicationContext(), PaymentVerification.class)
                    .putExtra("Amount", et1.getText().toString())
                    .putExtra("Name", name)
                    .putExtra("life_token", life_token)
                    .putExtra("eth", eth_token)
                    .putExtra("COINSTATUS", COINSTATUS)
            );
            finish();
        }else {
            Toast.makeText(SendActivity1.this,"Please fill amount",Toast.LENGTH_LONG).show();

        }

    }


    public void MenuNow(View v){
        try{
            if(dialog!=null && dialog.isShowing()) {
                dialog.dismiss();

            }
            dialog = new Dialog(SendActivity1.this);
                dialog.setCancelable(true);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.my_list);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

                RecyclerView lv2 = dialog.findViewById(R.id.lv1);

            l1 = new ArrayList<>();
            l1.add(new HomeDto("LIFETOKEN",R.drawable.bitcoin));
            l1.add(new HomeDto("ETH",R.drawable.bitcoin));

            HomeAdp2 homeAdp1 = new HomeAdp2(SendActivity1.this, l1, new HomeAdp2.OnItemClickListener() {
                @Override
                public void onItemClick(int item) {

                    ((ImageView)findViewById(R.id.icon1)).setImageResource(l1.get(item).getImg());
                    ((TextView)findViewById(R.id.menu_txt)).setText(l1.get(item).getTitle());
                    if(dialog!=null && dialog.isShowing()) {
                        dialog.dismiss();
                    }
                    if(l1.get(item).getTitle().equals("LIFETOKEN")){
                        t2.setText(life_token);
                        WalletRequest("LIFETOKEN");
                        COINSTATUS = "LIFETOKEN";
                    }else {
                        t2.setText(eth_token);
                        WalletRequest("ETH");
                        COINSTATUS = "ETH";
                    }



                }
            });

            LinearLayoutManager layoutManager1 = new LinearLayoutManager(SendActivity1.this, LinearLayoutManager.VERTICAL, false);
            lv2.setLayoutManager(layoutManager1);
            lv2.setItemAnimator(new DefaultItemAnimator());
            lv2.setNestedScrollingEnabled(false);
            lv2.setAdapter(homeAdp1);
                dialog.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    public void FinNow(View v){
        finish();
    }

    @Override
    public void onClick(int pos) {

        ((ImageView)findViewById(R.id.icon1)).setImageResource(l1.get(pos).getImg());
        ((TextView)findViewById(R.id.menu_txt)).setText(l1.get(pos).getTitle());
        if(dialog!=null && dialog.isShowing()) {
            dialog.dismiss();
        }
        if(l1.get(pos).getTitle().equals("LIFETOKEN")){
            t2.setText(life_token);
            WalletRequest("LIFETOKEN");
            COINSTATUS = "LIFETOKEN";
        }else {
            t2.setText(eth_token);
            WalletRequest("ETH");
            COINSTATUS = "ETH";
        }
    }


    private void WalletRequest(String tokenname) {
        final KProgressHUD progressDialog = KProgressHUD.create(SendActivity1.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        WalletRequest loginRequest = new WalletRequest(new Response.Listener<String>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONObject objectresults = object.getJSONObject("results");
                        JSONObject objectlifeToken = objectresults.getJSONObject("lifeToken");
                        JSONObject objecteth = objectresults.getJSONObject("eth");
                        et1.setText("");
                        if(tokenname.equals("LIFETOKEN")){
                            total = objectlifeToken.getString("balance");
                            t4.setText("Balance : "+total+" USD");
                        }else {
                            total = objecteth.getString("balance");
                            t4.setText("Balance : "+total+" USD");
                        }

                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(SendActivity1.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SendActivity1.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(SendActivity1.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(SendActivity1.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(SendActivity1.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }

}
