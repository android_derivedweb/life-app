package com.laundry.lifetest.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.StakingByIDRequest;
import com.laundry.lifetest.API.StakingRequest;
import com.laundry.lifetest.Model.StakingModel;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class DefiAc1 extends AppCompatActivity {


    private UserSession userSession;
    private RequestQueue requestQueue;
    private TextView amount,date,dayspassed,duration,interest_rate,profit;

    private RelativeLayout layout1, layout2, layout3, layout4, layout5;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_defi1);


        findViewById(R.id.nvsjdvnj).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openEditDialog();
            }
        });

        userSession = new UserSession(DefiAc1.this);
        requestQueue = Volley.newRequestQueue(DefiAc1.this);//Creating the RequestQueue


        amount = findViewById(R.id.amount);
        date = findViewById(R.id.date);
        dayspassed = findViewById(R.id.dayspassed);
        duration = findViewById(R.id.duration);
        interest_rate = findViewById(R.id.interest_rate);
        profit = findViewById(R.id.profit);


        String newString;
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                newString= null;
            } else {
                newString= extras.getString("StakId");
            }
        } else {
            newString= (String) savedInstanceState.getSerializable("StakId");
        }

        Log.e("StakId",newString);
        StakingByIdRequest(newString);

    }



    private void openEditDialog() {
        BottomSheetDialog dialog = new BottomSheetDialog(DefiAc1.this, R.style.BottomSheetDialog);
        dialog.setContentView(R.layout.dialog_wallet_connect);


        layout1 = dialog.findViewById(R.id.layout1);
        layout2 = dialog.findViewById(R.id.layout2);
        layout3 = dialog.findViewById(R.id.layout3);
        layout4 = dialog.findViewById(R.id.layout4);
        layout5 = dialog.findViewById(R.id.layout5);


        layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layout1.setBackground(getResources().getDrawable(R.drawable.color_round_blue_10dp));
                layout2.setBackground(null);
                layout3.setBackground(null);
                layout4.setBackground(null);
                layout5.setBackground(null);

                openInnerDialog("1");

            }
        });

        layout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layout1.setBackground(null);
                layout2.setBackground(getResources().getDrawable(R.drawable.color_round_blue_10dp));
                layout3.setBackground(null);
                layout4.setBackground(null);
                layout5.setBackground(null);

                openInnerDialog("2");

            }
        });

        layout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layout1.setBackground(null);
                layout2.setBackground(null);
                layout3.setBackground(getResources().getDrawable(R.drawable.color_round_blue_10dp));
                layout4.setBackground(null);
                layout5.setBackground(null);

                openInnerDialog("3");

            }
        });

        layout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layout1.setBackground(null);
                layout2.setBackground(null);
                layout3.setBackground(null);
                layout4.setBackground(getResources().getDrawable(R.drawable.color_round_blue_10dp));
                layout5.setBackground(null);

                openInnerDialog("4");

            }
        });

        layout5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layout1.setBackground(null);
                layout2.setBackground(null);
                layout3.setBackground(null);
                layout4.setBackground(null);
                layout5.setBackground(getResources().getDrawable(R.drawable.color_round_blue_10dp));

                openInnerDialog("5");

            }
        });


        dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    private void openInnerDialog(String pos) {
        BottomSheetDialog dialog = new BottomSheetDialog(DefiAc1.this, R.style.BottomSheetDialog);
        dialog.setContentView(R.layout.dialog_wallet_inner);


        TextView title = dialog.findViewById(R.id.title);
        ImageView ok = dialog.findViewById(R.id.ok);

        if (pos.equals("1")){
            title.setText("Metamask");
            ok.setBackgroundResource(R.drawable.fox);
        } else if (pos.equals("2")){
            title.setText("WalletConnect");
            ok.setBackgroundResource(R.drawable.wifi_ic);
        } else if (pos.equals("3")){
            title.setText("Coinbase Wallet");
            ok.setBackgroundResource(R.drawable.coinbase);
        } else if (pos.equals("4")){
            title.setText("Formatic");
            ok.setBackgroundResource(R.drawable.formatic);
        } else if (pos.equals("5")){
            title.setText("Portis");
            ok.setBackgroundResource(R.drawable.portis);
        }


        dialog.findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }





    private void StakingByIdRequest(String id) {
        final KProgressHUD progressDialog = KProgressHUD.create(DefiAc1.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        StakingByIDRequest loginRequest = new StakingByIDRequest(id,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONObject resultstransactions = object.getJSONObject("results");
                        //    JSONArray resultstransactions = objectresults.getJSONArray("transactions");

                       /* mName.setText(resultstransactions.getString("lockInPeriod"));
                        mSubName.setText(resultstransactions.getString("currency"));*/
                        interest_rate.setText(resultstransactions.getString("roi")+"%");
                        duration.setText(resultstransactions.getString("duration"));
                       // dayspassed.setText(resultstransactions.getString("duration"));
                        date.setText(resultstransactions.getString("startDate"));
                        amount.setText(resultstransactions.getString("amount"));

                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(DefiAc1.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(DefiAc1.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(DefiAc1.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(DefiAc1.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(DefiAc1.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }



    public void NextNow(View v){
        startActivity(new Intent(getApplicationContext(),AirDropAc.class));
        finish();
    }
    public void FinNow(View v){
        finish();
    }


}
