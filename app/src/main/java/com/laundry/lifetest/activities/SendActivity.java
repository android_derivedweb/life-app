package com.laundry.lifetest.activities;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.AllLifeUsersRequest;
import com.laundry.lifetest.API.StakingRequest;
import com.laundry.lifetest.ItemClick;
import com.laundry.lifetest.Model.Contact;
import com.laundry.lifetest.Model.LifeAppUser;
import com.laundry.lifetest.Model.StakingModel;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;
import com.laundry.lifetest.adapters.HomeAdp;
import com.laundry.lifetest.adapters.HomeAdp1;
import com.laundry.lifetest.adapters.SendAdp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class SendActivity extends AppCompatActivity implements ItemClick {

    private RecyclerView lv1;
    private ArrayList<LifeAppUser> lifeAppUsers = new ArrayList<LifeAppUser>();
    private UserSession userSession;
    private RequestQueue requestQueue;
    private SendAdp homeAdp1;

    private TextView noData;

    private static final String[] PROJECTION = new String[]{
            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
            ContactsContract.Contacts.DISPLAY_NAME,
            ContactsContract.CommonDataKinds.Phone.NUMBER
    };


    ArrayList<Contact> contactList = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send);

        userSession = new UserSession(SendActivity.this);
        requestQueue = Volley.newRequestQueue(SendActivity.this);//Creating the RequestQueue

        initUI();
        addAdp();

        noData = findViewById(R.id.noData);



        AllLifeUsersRequest();


        getContactList();


    }


    public void NextNow(View v){
//        startActivity(new Intent(getApplicationContext(),SendActivity1.class));
//        finish();
    }
    public void NextNow1(View v){
        startActivity(new Intent(getApplicationContext(),SendOther1.class));
        finish();
    }
    private void addAdp() {



        homeAdp1 = new SendAdp(SendActivity.this,lifeAppUsers);

        LinearLayoutManager layoutManager1
                = new LinearLayoutManager(SendActivity.this, LinearLayoutManager.VERTICAL, false);
        lv1.setLayoutManager(layoutManager1);
        lv1.setItemAnimator(new DefaultItemAnimator());
        lv1.setNestedScrollingEnabled(false);
        lv1.setAdapter(homeAdp1);
    }
    private void initUI() {
        lv1 = findViewById(R.id.lv1);
    }


    public void FinNow(View v){
        finish();
    }

    @Override
    public void onClick(int pos) {
        startActivity(new Intent(getApplicationContext(),SendActivity1.class)
        .putExtra("id",lifeAppUsers.get(pos).get_id())
        .putExtra("name",lifeAppUsers.get(pos).getName())
        .putExtra("eth_token",lifeAppUsers.get(pos).getEthWalletaddress())
        .putExtra("life_token",lifeAppUsers.get(pos).getLifeTokenWalletaddress()));
    }




    private void getContactList() {
        ContentResolver cr = getContentResolver();

        Cursor cursor = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, PROJECTION, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        if (cursor != null) {
            HashSet<String> mobileNoSet = new HashSet<String>();
            try {
                final int nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
                final int numberIndex = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

                String name, number;
                while (cursor.moveToNext()) {
                    name = cursor.getString(nameIndex);
                    number = cursor.getString(numberIndex);
                    number = number.replace(" ", "");
                    if (!mobileNoSet.contains(number)) {
                        contactList.add(new Contact(name, number));
                        mobileNoSet.add(number);
                        Log.d("hvy", "onCreaterrView  Phone Number: name = " + name
                                + " No = " + number);
                    }
                }
            } finally {
                cursor.close();
            }
        }

        Log.e("phoneNo", contactList.size() + "");
    }


    private void AllLifeUsersRequest() {
        final KProgressHUD progressDialog = KProgressHUD.create(SendActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        AllLifeUsersRequest loginRequest = new AllLifeUsersRequest(new Response.Listener<String>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONArray resultstransactions = object.getJSONArray("results");
                        //    JSONArray resultstransactions = objectresults.getJSONArray("transactions");

                        for (int i = 0; i < resultstransactions.length(); i++) {
                            JSONObject jsonObject1 = resultstransactions.getJSONObject(i);
                            LifeAppUser lifeAppUser = new LifeAppUser();
                            lifeAppUser.setEthWalletaddress(jsonObject1.getJSONObject("ethWallet").getString("address"));
                            lifeAppUser.setLifeTokenWalletaddress(jsonObject1.getJSONObject("lifeTokenWallet").getString("address"));
                            lifeAppUser.setEthWalletcurrencyl(jsonObject1.getJSONObject("ethWallet").getString("currency"));
                            lifeAppUser.setLifeTokenWalletcurrency(jsonObject1.getJSONObject("lifeTokenWallet").getString("currency"));
                            lifeAppUser.set_id(jsonObject1.getString("_id"));
                            lifeAppUser.setName(jsonObject1.getString("name"));
                            lifeAppUser.setPhone(jsonObject1.getString("phone"));


                            for (int j = 0; j < contactList.size(); j++) {
                                String phone = contactList.get(j).getPhoneNumber();
                                String strLastFourDi = phone.length() >= 10 ? phone.substring(phone.length() - 10): "";

                                if (jsonObject1.getString("phone").equals(strLastFourDi))
                                    lifeAppUsers.add(lifeAppUser);
                            }

                            if (lifeAppUsers.isEmpty()){
                                noData.setVisibility(View.VISIBLE);
                            } else {
                                noData.setVisibility(View.GONE);
                            }

                        }

                        homeAdp1.notifyDataSetChanged();

                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(SendActivity.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SendActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(SendActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(SendActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(SendActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }

    

}
