package com.laundry.lifetest.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.tabs.TabLayout;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.TransactionRequest;
import com.laundry.lifetest.API.WalletRequest;
import com.laundry.lifetest.Model.GraphModel;
import com.laundry.lifetest.ItemClick;
import com.laundry.lifetest.Model.TransactionModel;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;
import com.laundry.lifetest.adapters.HomeAdp;
import com.laundry.lifetest.adapters.HomeAdp1;
import com.laundry.lifetest.adapters.HomeAdp3;
import com.laundry.lifetest.dto.HomeDto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeScreen extends AppCompatActivity implements ItemClick {

    private TabLayout tabs;
    private RecyclerView lv1,lv2;
    private JSONArray l1 = new JSONArray();
    List<HomeDto> l2 = new ArrayList<>();
    private List<HomeDto> tab_options = new ArrayList<>();
    private BottomSheetDialog dialog = null;
    private UserSession userSession;
    private RequestQueue requestQueue;
    private TextView total_wallet;
    ArrayList<GraphModel> modelArrayList = new ArrayList<>();
    ArrayList<TransactionModel> TransactionArrayList = new ArrayList<>();
    private HomeAdp1 homeAdp1;


    private static final int REQUEST = 112;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        userSession = new UserSession(HomeScreen.this);
        requestQueue = Volley.newRequestQueue(HomeScreen.this);//Creating the RequestQueue



        initUI();
        addAdp();
        setupTabs();


        total_wallet = findViewById(R.id.total_wallet);
        TransactionRequest();
        WalletRequest();

        Log.e("TOken",userSession.getAPIToken());
      
    }
    private void setupTabs() {
        try {
            tabs.removeAllTabs();
            tab_options = new ArrayList<>();

            tab_options.add(new HomeDto(getString(R.string.air_drop), R.drawable.wifi_ic1));
            tab_options.add(new HomeDto(getString(R.string.home), R.drawable.home_ic));
            tab_options.add(new HomeDto(getString(R.string.defi), R.drawable.nva_ic1));
            for (int i = 0; i < tab_options.size(); i++) {
                tabs.addTab(tabs.newTab());
                RelativeLayout tabOne = (RelativeLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab1, null);
                TextView txt1 = tabOne.findViewById(R.id.txt1);
                TextView txt2 = tabOne.findViewById(R.id.txt2);
                ImageView imv = tabOne.findViewById(R.id.imv);
                ImageView imv1 = tabOne.findViewById(R.id.imv1);


                RelativeLayout card1 = tabOne.findViewById(R.id.card1);
                RelativeLayout card2 = tabOne.findViewById(R.id.card2);

                txt1.setText(tab_options.get(i).getTitle());
                txt2.setText(tab_options.get(i).getTitle());

                imv.setImageResource(tab_options.get(i).getImg());
                imv1.setImageResource(tab_options.get(i).getImg());

                card1.setVisibility(View.VISIBLE);
                card2.setVisibility(View.VISIBLE);
                tabs.getTabAt(i).setCustomView(tabOne);
            }
            setCurrTab(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setCurrTab(int pos) {
        try {
            for (int i = 0; i < tab_options.size(); i++) {
                RelativeLayout tabOne = (RelativeLayout) tabs.getTabAt(i).getCustomView();
                RelativeLayout card1 = tabOne.findViewById(R.id.card1);
                RelativeLayout card2 = tabOne.findViewById(R.id.card2);
                if (pos == i) {
                    card1.setVisibility(View.GONE);
                    card2.setVisibility(View.VISIBLE);
                } else {
                    card1.setVisibility(View.VISIBLE);
                    card2.setVisibility(View.GONE);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void SendNow(View v){

        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {android.Manifest.permission.READ_CONTACTS};
            if (!hasPermissions(HomeScreen.this, PERMISSIONS)) {
                ActivityCompat.requestPermissions(HomeScreen.this, PERMISSIONS, REQUEST );
            } else {
                startActivity(new Intent(getApplicationContext(),SendActivity.class));
            }
        } else {
            startActivity(new Intent(getApplicationContext(),SendActivity.class));
        }

    }


    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(getApplicationContext(),SendActivity.class));
                } else {
                    Toast.makeText(HomeScreen.this, "Give permission in app setting!", Toast.LENGTH_LONG).show();
                }
            }
        }
    }



    public void MenuNow(View v){
        try{
            if(dialog!=null && dialog.isShowing()) {
                dialog.dismiss();

            }
            dialog = new BottomSheetDialog(HomeScreen.this);
            dialog.setCancelable(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.my_list1);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

            RecyclerView lv2 = dialog.findViewById(R.id.lv1);

            l2 = new ArrayList<>();
            l2.add(new HomeDto("Profile",R.drawable.user_ic));
            l2.add(new HomeDto("Update Password",R.drawable.user_ic));
            l2.add(new HomeDto("History",R.drawable.time_ic));
            l2.add(new HomeDto("Time Machine",R.drawable.clock_ic));
            l2.add(new HomeDto("Feedback",R.drawable.feed_ic));



            HomeAdp3 homeAdp1 = new HomeAdp3(HomeScreen.this,l2);

            LinearLayoutManager layoutManager1
                    = new LinearLayoutManager(HomeScreen.this, LinearLayoutManager.VERTICAL, false);
            lv2.setLayoutManager(layoutManager1);
            lv2.setItemAnimator(new DefaultItemAnimator());
            lv2.setNestedScrollingEnabled(false);
            lv2.setAdapter(homeAdp1);
            dialog.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    private void addAdp() {


        GraphModel graphModel = new GraphModel();
        graphModel.setName("Eth (USDT)");
        graphModel.setPrice("$"+userSession.getETHBalance());
        modelArrayList.add(graphModel);

        GraphModel graphModel1 = new GraphModel();
        graphModel1.setName("LifeToken (USDT)");
        graphModel1.setPrice("$"+userSession.getLIFEBalance());
        modelArrayList.add(graphModel1);

        HomeAdp homeAdp = new HomeAdp(HomeScreen.this,modelArrayList);

        LinearLayoutManager layoutManager
                = new LinearLayoutManager(HomeScreen.this, LinearLayoutManager.HORIZONTAL, false);
        lv1.setLayoutManager(layoutManager);
        lv1.setItemAnimator(new DefaultItemAnimator());
        lv1.setNestedScrollingEnabled(false);
        lv1.setAdapter(homeAdp);

        homeAdp1 = new HomeAdp1(HomeScreen.this,TransactionArrayList);

        LinearLayoutManager layoutManager1
                = new LinearLayoutManager(HomeScreen.this, LinearLayoutManager.VERTICAL, false);
        lv2.setLayoutManager(layoutManager1);
        lv2.setItemAnimator(new DefaultItemAnimator());
        lv2.setNestedScrollingEnabled(false);
        lv2.setAdapter(homeAdp1);
    }

    private void initUI() {
        tabs = findViewById(R.id.tabs);
        lv1 = findViewById(R.id.lv1);
        lv2 = findViewById(R.id.lv2);

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if(tab.getPosition()==0){
                    startActivity(new Intent(getApplicationContext(),AirDropAc.class));
                }
                else if(tab.getPosition()==2){
                    startActivity(new Intent(getApplicationContext(),DefiAc.class));
                }
            }
        });
    }

    @Override
    public void onClick(int pos) {
        switch (pos){
            case 0:
                startActivity(new Intent(getApplicationContext(),ProfileActivity.class));
                break;
                case 1:
                    startActivity(new Intent(getApplicationContext(),SetNewPasswordActivity.class));
                    break;

                case 2:
                    startActivity(new Intent(getApplicationContext(),HistoryAc.class));
                    break;
        }
    }

    private void TransactionRequest() {
        final KProgressHUD progressDialog = KProgressHUD.create(HomeScreen.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        TransactionRequest loginRequest = new TransactionRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONArray resultstransactions = object.getJSONArray("results");
                    //    JSONArray resultstransactions = objectresults.getJSONArray("transactions");

                        for (int i = 0; i < resultstransactions.length(); i++) {
                            JSONObject jsonObject1 = resultstransactions.getJSONObject(i);
                            TransactionModel transactionModel = new TransactionModel();
                            transactionModel.setAmount(jsonObject1.getString("amount"));
                            transactionModel.setCurrency(jsonObject1.getString("currency"));
                            transactionModel.setFrom(jsonObject1.getString("from"));
                            transactionModel.setTo(jsonObject1.getString("to"));
                            transactionModel.setTxHash(jsonObject1.getString("TxHash"));
                            transactionModel.setTxTime(jsonObject1.getString("txTime"));
                            transactionModel.setTxType(jsonObject1.getString("txType"));
                            transactionModel.setStatus(jsonObject1.getString("status"));
                            transactionModel.setUSD("$"+jsonObject1.getString("usd"));
                            transactionModel.setName(jsonObject1.getString("name"));
                            TransactionArrayList.add(transactionModel);
                        }
                        homeAdp1.notifyDataSetChanged();
                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(HomeScreen.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(HomeScreen.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(HomeScreen.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(HomeScreen.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(HomeScreen.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }
    private void WalletRequest() {
        final KProgressHUD progressDialog = KProgressHUD.create(HomeScreen.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        WalletRequest loginRequest = new WalletRequest(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");
                        JSONObject objectresults = object.getJSONObject("results");

                        String total = objectresults.getString("total").replace(" USD","");
                        total_wallet.setText(total);
                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(HomeScreen.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(HomeScreen.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(HomeScreen.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(HomeScreen.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(HomeScreen.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }

    

}
