package com.laundry.lifetest.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.laundry.lifetest.API.SetNewPasswordRequest;
import com.laundry.lifetest.API.SetNewPasswordRequest2;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SetNewPasswordActivity extends AppCompatActivity {


    private EditText passowrd;
    private UserSession userSession;
    private RequestQueue requestQueue;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newpass);

        passowrd = findViewById(R.id.passowrd);


        userSession = new UserSession(SetNewPasswordActivity.this);
        requestQueue = Volley.newRequestQueue(SetNewPasswordActivity.this);//Creating the RequestQueue



        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (passowrd.getText().toString().isEmpty()){
                    Toast.makeText(SetNewPasswordActivity.this, "Please enter password", Toast.LENGTH_SHORT).show();
                } else {
                    setNewPass(passowrd.getText().toString());
                }
            }
        });

    }


    private void setNewPass(String password) {
        final KProgressHUD progressDialog = KProgressHUD.create(SetNewPasswordActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.5f)
                .show();

        SetNewPasswordRequest2 loginRequest = new SetNewPasswordRequest2(password, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.e("response",response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getBoolean("success")) {
                        JSONObject object = jsonObject.getJSONObject("data");

                        Toast.makeText(SetNewPasswordActivity.this, object.getString("msg"), Toast.LENGTH_SHORT).show();

                        finish();
                    }else {
                        JSONObject object = jsonObject.getJSONObject("data");
                        Toast.makeText(SetNewPasswordActivity.this, object.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SetNewPasswordActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                progressDialog.dismiss();
                if (error instanceof ServerError)
                    Toast.makeText(SetNewPasswordActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                else if (error instanceof TimeoutError)
                    Toast.makeText(SetNewPasswordActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                else if (error instanceof NetworkError)
                    Toast.makeText(SetNewPasswordActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                params.put("Authorization","Bearer "+ userSession.getAPIToken());
                return params;
            }};
        loginRequest.setTag("TAG");
        loginRequest.setShouldCache(false);
        requestQueue.add(loginRequest);
    }




    public void NextNow(View v){
        finish();
    }


    public void FinNow(View v){
        finish();
    }


}
