package com.laundry.lifetest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.laundry.lifetest.R;

public class VerifyTrans2 extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vtran2);
    }

    public void NextNow(View v){
        startActivity(new Intent(getApplicationContext(),DefiAc.class));
        finish();
    }

    public void FinNow(View v){
        finish();
    }
}
