package com.laundry.lifetest.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.laundry.lifetest.R;
import com.laundry.lifetest.Utils.UserSession;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        UserSession userSession= new UserSession(SplashScreen.this);

        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Log.w("TAG", "Fetching FCM registration token failed", task.getException());
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                       // session.setFirbaseDeviceToken(token);
                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        Log.d("TAG", msg);

                    }
                });

        new Thread(){
            @Override
            public void run() {
                try{
                    sleep(2000);

                    if(userSession.isLoggedIn()){
                        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                        finish();
                    }else {
                        startActivity(new Intent(getApplicationContext(), SplashScreen1.class));
                        finish();
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        }.start();
    }



}
