package com.laundry.lifetest.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.core.app.NotificationCompat;

public class UserSession {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    public static String BASEURL1 = "https://lifetoken.herokuapp.com/api/v1/users/";
    public static String BASEURL = "https://lifetoken-test.herokuapp.com/api/v1/users/";

    private static final String PREF_NAME = "Life";

    private static final String IS_LOGIN = "IsLogin";
    private static final String LIFE_NAME = "LIFE_NAME";
    private static final String LIFE_EMAIL = "LIFE_EMAIL";
    private static final String LIFE_PHONE = "LIFE_PHONE";
    private static final String LIFE_ETHADDRESS = "LIFE_ETHADDRESS";
    private static final String LIFE_ETHBALANCE = "LIFE_ETHBALANCE";
    private static final String LIFE_TOKENADDRESS = "LIFE_TOKENADDRESS";
    private static final String LIFE_TOKENBALANCE = "LIFE_TOKENBALANCE";
    private static final String LIFE_TOKEN = "LIFE_TOKEN";



    public UserSession(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void createLoginSession(String name,
                                   String email,
                                   String phone,
                                   String ethAddress,
                                   String ethBalance,
                                   String lifeTokenAddress,
                                   String lifeTokenBalance,
                                   String token
    ) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        // Storing name in pref
        editor.putString(LIFE_NAME, name);
        editor.putString(LIFE_EMAIL, email);
        editor.putString(LIFE_PHONE, phone);
        editor.putString(LIFE_ETHADDRESS, ethAddress);
        editor.putString(LIFE_ETHBALANCE, ethBalance);
        editor.putString(LIFE_TOKENADDRESS, lifeTokenAddress);
        editor.putString(LIFE_TOKENBALANCE, lifeTokenBalance);
        editor.putString(LIFE_TOKEN, token);
        editor.commit();
    }


    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public String getAPIToken() {
        return sharedPreferences.getString(LIFE_TOKEN, "");
    }

    public String getETHAddress() {
        return sharedPreferences.getString(LIFE_ETHADDRESS, "");
    }

    public String getETHBalance() {
        return sharedPreferences.getString(LIFE_ETHBALANCE, "");
    }

    public String getLIFEBalance() {
        return sharedPreferences.getString(LIFE_TOKENBALANCE, "");
    }

    public String getLIFEAddress() {
        return sharedPreferences.getString(LIFE_TOKENADDRESS, "");
    }



    public boolean logout() {
        return sharedPreferences.edit().clear().commit();
    }

}
