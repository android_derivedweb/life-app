package com.laundry.lifetest.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.laundry.lifetest.Utils.UserSession;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class SetNewPasswordRequest extends StringRequest {

    private Map<String, String> parameters;

    public SetNewPasswordRequest(String phone, String password, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.PATCH, UserSession.BASEURL+"forgot-password", listener, errorListener);
        parameters = new HashMap<>();
        parameters.put("phone", phone);
        parameters.put("password", password);
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
