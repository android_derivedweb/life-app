package com.laundry.lifetest.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.laundry.lifetest.Utils.UserSession;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class UpdateProfile extends StringRequest {

    private Map<String, String> parameters;


    public UpdateProfile(String name, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.PATCH, UserSession.BASEURL+"update-profile", listener, errorListener);
        parameters = new HashMap<>();
        parameters.put("name", name);
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
