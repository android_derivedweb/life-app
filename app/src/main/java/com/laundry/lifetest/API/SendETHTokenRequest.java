package com.laundry.lifetest.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.laundry.lifetest.Utils.UserSession;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class SendETHTokenRequest extends StringRequest {

    private Map<String, String> parameters;


    public SendETHTokenRequest(String amount, String receiverAddress, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, UserSession.BASEURL+"send-eth", listener, errorListener);
        parameters = new HashMap<>();
        parameters.put("amount", amount);
        parameters.put("receiverAddress", "0x4f048699ff2fc1f4306cfc77acae6f1294f5fffd");


    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
