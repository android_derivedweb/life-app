package com.laundry.lifetest.API;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.laundry.lifetest.Utils.UserSession;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kshitij on 12/17/17.
 */

public class AddUserRequest extends StringRequest {

    private Map<String, String> parameters;


    public AddUserRequest(String currency, String userName, String address, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, UserSession.BASEURL+"add-other-user", listener, errorListener);
        parameters = new HashMap<>();
        parameters.put("currency", currency);
        parameters.put("userName", userName);
        parameters.put("address", address);
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parameters;
    }

}
