package com.laundry.lifetest.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.laundry.lifetest.R;

public class Home extends Fragment {

    private int image;
    private ImageView imv;
    private View v;

    public Home(int image) {
        this.image = image;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.banner_item, container, false);
        imv = v.findViewById(R.id.imv);
        imv.setImageResource(image);
        return v;
    }
}
