package com.laundry.lifetest.dto;

import androidx.fragment.app.Fragment;

/*
* normal pojo classes with cunstructors and setter/getter
* for Home page bottom navigation
* */
public class HomeDto {
    private String title;
    private int img;

    public HomeDto() {
    }

    public HomeDto(String title, int img) {
        this.title = title;
        this.img = img;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
